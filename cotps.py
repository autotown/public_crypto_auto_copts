from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

import time

import os
from dotenv import load_dotenv

load_dotenv()

MY_URL = 'https://cotps.com/#/pages/transaction/transaction'

MY_COUNTRY_CODE = os.getenv('MY_COUNTRY_CODE')
MY_PHONE = os.getenv('MY_PHONE')
MY_PASS = os.getenv('MY_PASS')


options = Options()
options.headless = True
driver = webdriver.Firefox(options=options)

def do_loop(refresh=True):
	print('Lets get Ready to Run')
	if refresh:
		driver.refresh()
		print('Refreshing Drivers')
		time.sleep(15)

	print('Checking Balance')
	moneys = driver.find_elements(By.CLASS_NAME, 'division-num')
	if not len(moneys) > 1 or not moneys[1].text:
		print('Balance Not Found, Retrying...')
		return do_loop()

	my_money = float(moneys[1].text)
	print(f'\nmy current balance is {my_money}')
	time.sleep(3)

	if my_money > 5:
		print('selling...')
		driver.find_element(by=By.CLASS_NAME, value='orderBtn').click()
		time.sleep(15)

		driver.find_element(By.XPATH, '//uni-button[text()="Sell"]').click()
		time.sleep(15)
		print('sold successfully! re-running loop...')
		return do_loop()
	else:
		print('can\'t sell anything. exiting loop...')
		return my_money

def do_start():
	print('Load Drivers')
	global driver
	if not driver:
		driver = webdriver.Firefox(options=options)
	print('Load COPTS')
	driver.get(MY_URL)
	print('\nstarting up...')
	time.sleep(5)

	driver.find_element(by=By.CLASS_NAME, value='uni-page-head__title').click()
	time.sleep(5)

	print(f'\nstart logging in for: {MY_PHONE}...')

	uni_texts = driver.find_elements(By.TAG_NAME, 'uni-text')
	uni_texts[1].click()
	time.sleep(5)

	driver.find_element(By.CLASS_NAME, 'uni-input-input').send_keys(MY_COUNTRY_CODE)
	time.sleep(2)
	driver.find_element(By.TAG_NAME, 'uni-button').click()
	time.sleep(2)

	inputs = driver.find_elements(By.CLASS_NAME, 'uni-input-input')
	inputs[0].send_keys(MY_PHONE)
	time.sleep(2)
	inputs[1].send_keys(MY_PASS)
	time.sleep(2)
	print('\ninputs sent! logging in...')

	inputs = driver.find_element(By.CLASS_NAME, 'login').click()
	time.sleep(8)
	print('\nlogged in! heading to transaction hall...')

	driver.get(MY_URL)
	time.sleep(15)
	print('\nin transaction hall! starting loop...')
	balance_left = do_loop(False)

	print(f'\nfinishing up... balance remaining: {balance_left}')
	driver.close()

try:
	do_start()
except Exception as e:
	print(f'\nsomething went wrong:\n {e}\n\nrestarting...')
	if driver:
		driver.close()
		driver = None
	do_start()






