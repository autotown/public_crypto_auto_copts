#!/bin/sh

sudo apt-get -y update &&
sudo apt -y install python3-pip &&
sudo pip install selenium &&
sudo apt -y install firefox-geckodriver &&
sudo pip install python-dotenv &&
sudo apt-get -y install npm &&
sudo npm install pm2@latest -g