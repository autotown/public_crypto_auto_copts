#!/bin/bash

NAME=${4:cotps}
CONFIG_FOLDER='.'${NAME}'_auto'
mkdir -p $CONFIG_FOLDER
cd $CONFIG_FOLDER || exit
curl https://gitlab.com/autotown/public_crypto_auto_copts/-/raw/master/cotps.py > cotps.py
curl https://gitlab.com/autotown/public_crypto_auto_copts/-/raw/master/cotps.sh > cotps.sh
curl https://gitlab.com/autotown/public_crypto_auto_copts/-/raw/master/setup.sh > setup.sh

echo -e "
MY_COUNTRY_CODE=$1
MY_PHONE=$2
MY_PASS=$3
" > .env

bash setup.sh
pm2 start cotps.sh --name $NAME